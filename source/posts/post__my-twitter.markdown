---
date: '2020-09-15 22:48:49'
title: My Twitter
titleCodeSafe: my-twitter
description: "Learn more about professional design and development best practices."
permalink: content/my-twitter/
canonical: https://about.brandonmlyon.com/content/my-twitter/
previousUrl: /content/my-pinterest/
nextUrl: /content/technical-writing/
layoutClass: box1of1
primaryColor: "#1da1f2"
tags:
  - all-posts
---
<h1>My Twitter</h1>

Here's a sample tweet from [my Twitter account](https://twitter.com/brandon_m_lyon). I discuss a wide range of topics related to website development, design, and management.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">This is a good coding tip <a href="https://t.co/lmCLiNp1ku">https://t.co/lmCLiNp1ku</a></p>&mdash; BrandonMLyon (@brandon_m_lyon) <a href="https://twitter.com/brandon_m_lyon/status/1432816445738524674?ref_src=twsrc%5Etfw">August 31, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
