---
date: '2020-09-15 22:26:35'
title: About me
titleCodeSafe: about-me
description: Learn about Brandon M Lyon's interests and how he works.
permalink: content/about-me/
canonical: https://about.brandonmlyon.com/content/about-me/
previousUrl: /content/apparel-e-commerce/
nextUrl: /content/about-this-website/
layoutClass: box1of1
primaryColor: "#e41b5b"
tags:
  - all-posts
---
# About me

I love to research, plan, and create. I can't imagine doing anything else with my life. I enjoy discussing trends, learning new techniques, and figuring out how things are made. If I had to pick a few of my favorite things they would be CMYK, frisbees, volleyball, and video games.

Read more about <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/brandon-lyon-readme.md#how-i-think" target="_blank" rel="noopener">how I think</a>, <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/brandon-lyon-readme.md#working-with-me" target="_blank" rel="noopener">working with me</a>, and some of <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/brandon-lyon-readme.md#favorite-quotes" target="_blank" rel="noopener">my favorite quotes</a>.