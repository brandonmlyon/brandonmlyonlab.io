---
date: '2020-09-15 17:11:16'
title: The door problem
titleCodeSafe: the-door-problem
description: Someone has to specify how a door works. It's not as obvious as you
  might think.
permalink: content/the-door-problem/
canonical: https://about.brandonmlyon.com/content/the-door-problem/
previousUrl: /content/project-management/
nextUrl: /content/a-software-tester-walks-into-a-bar/
layoutClass: box1of1 boxQuote
primaryColor: "#e41b5b"
tags:
  - all-posts
---
## ***Recommended reading***

"Design is a nebulous term. I like to describe the "The Door Problem." Someone has to specify how a door works. It's not as obvious as you might think."

<a href="http://www.lizengland.com/blog/2014/04/the-door-problem/" target="_blank" rel="noopener">🔗 The Door Problem</a>
