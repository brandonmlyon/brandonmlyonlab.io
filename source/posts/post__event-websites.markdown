---
date: '2020-10-06 20:49:51'
title: Event websites
titleCodeSafe: event-websites
description: Events evolve quickly and timing is everything.
permalink: content/event-websites/
canonical: https://about.brandonmlyon.com/content/event-websites/
previousUrl: /content/natural-language-ux/
nextUrl: /content/mobile-app-development/
layoutClass: box1of2
sidebar: true
primaryColor: "#E26700"
image: https://res.cloudinary.com/bml/image/upload/v1635544062/brandonmlyon/gl-events.jpg
tags:
  - all-posts
---
# Event websites &amp; live audiences

GitLab had event webpages that needed to be updated manually by engineers for several months per event. I was able to streamline and <a href="https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/200" target="_blank" rel="noopener">automate the process</a> by predictively swapping out reusable modules created in advance.
