---
date: '2020-09-15 22:34:57'
title: Marketing automation
titleCodeSafe: marketing-automation
description: I create bespoke landing pages, emails, and forms using tools like Marketo.
permalink: content/marketing-automation/
canonical: https://about.brandonmlyon.com/content/marketing-automation/
previousUrl: /content/analytics-dashboards/
nextUrl: /content/re-build-from-scratch/
layoutClass: box1of2
sidebar: true
primaryColor: "#5a54a4"
imageAltText: Screenshot of a Marketo landing page template for Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1635544122/brandonmlyon/ee-marketo.jpg
tags:
  - all-posts
---
# Marketing automation for lead generation & nurture

Marketing automation tools are important but they can be unintuitive. I've built campaigns for Codefresh, GitLab, Experts Exchange, and other customers using modern development techniques with platforms such as Marketo and Hubspot.
