---
date: '2020-09-15 03:30:57'
title: More than just a developer
titleCodeSafe: more-than-just-a-developer
description: What else can Brandon do?
permalink: content/not-just-a-developer/
canonical: https://about.brandonmlyon.com/content/not-just-a-frontend-developer/
previousUrl: /
nextUrl: /content/growth-marketing/
layoutClass: box1of1
primaryColor: "#f2c00e"
tags:
  - all-posts
---
# A well-rounded creator

My primary passion is working on websites. I'm a cross-discipline manager, full-stack developer, UX researcher, and designer for multiple industries. I have planned and created apps for mobile, desktop, wearables, mixed reality AR / VR, and voice assistants. Outside of software, I have experience in graphic design, apparel design, industrial design, architectural design, 3D CAD/CAM, audio, video, and photography.
