---
date: '2020-09-15 22:51:58'
title: Re-design and re-build from scratch
titleCodeSafe: re-design-and-re-build-from-scratch
description: How can we organize hundreds of thousands of lines of code?
permalink: content/re-build-from-scratch/
canonical: https://about.brandonmlyon.com/content/re-build-from-scratch/
previousUrl: /content/marketing-automation/
nextUrl: /content/content-management-systems-and-blogs/
layoutClass: box1of2
sidebar: true
primaryColor: "#00a3e0"
imageAltText: "Animation of homepage screenshots from Experts exchange and their redesigns over time"
image: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/ee-home.gif
tags:
  - all-posts
---
# Built from scratch on a tight schedule

Experts Exchange has a massive codebase and hundreds of page types. I've planned the frontend infrastructure and codebase several times over a decade. It takes many months of long nights to re-build the entire site from scratch.
