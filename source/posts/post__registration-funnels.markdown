---
date: '2020-09-15 20:59:10'
title: Registration funnels
titleCodeSafe: registration-funnels
description: Running a subscription company is more complicated than it looks.
permalink: content/registration-funnels/
canonical: https://about.brandonmlyon.com/content/registration-funnels/
previousUrl: /content/inbound-marketing-redesigns/
nextUrl: /content/a-b-testing-framework/
layoutClass: box1of2
sidebar: true
primaryColor: "#e26700"
imageAltText: Screenshot of the pricing page on Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/ee-register2018q4.jpg
tags:
  - all-posts
---
# Improve registration funnels to make money

Subscription companies need good payment funnels. We built a robust system for Experts Exchange with coupon codes, foreign currency, account credits, feature bundles, account tiers, subscription durations, account locking, declined payment systems, fraud prevention, friendly and specific error messages, GDPR support, and more.
