---
date: '2020-10-06 18:05:30'
title: Project management
titleCodeSafe: project-management
description: Use math for predictable project management results.
permalink: content/project-management/
canonical: https://about.brandonmlyon.com/content/project-management/
previousUrl: /content/technical-writing/
nextUrl: /content/the-door-problem/
layoutClass: box1of1 boxQuote
primaryColor: "#e26700"
tags:
  - all-posts
---
## ***Project management***

You have to know where you're going, how to get there, and how long it could take. Think about things one step at a time. Do the math to see how much you can accomplish in a quarter. Leave buffer room, and take PTO into account.

<a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/pepper-project-management.md" target="_blank" rel="noopener">🔗 Project Management Best Practices</a>
