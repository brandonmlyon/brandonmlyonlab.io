---
title: Last post
titleCodeSafe: last-post
description: You have reached the last post. Do you want to see more?
date: '2020-09-15 23:43:05'
permalink: content/last-post/
canonical: https://about.brandonmlyon.com/content/last-post/
previousUrl: /content/recommended-books/
layoutClass: box1of1 titleHighlight
primaryColor: "#e41b5b"
---
# Thanks for reading

## You have reached the *Last Post*.

If you want to read it again you can <a href="/">return to the start</a> or <a href="/tag/all-posts/">browse all posts</a>.