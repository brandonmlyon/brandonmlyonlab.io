---
date: '2020-09-14 23:01:14'
title: Contact
titleCodeSafe: contact
description: "Let's get together! I enjoy talking about tech. How can we help each other?"
permalink: contact
canonical: https://about.brandonmlyon.com/contact/
layoutClass: box1of1
primaryColor: '#e41b5b'
standalone: true
---

<style>
  .articleInnerWrap {
    width: inherit !important;
  }
  #contact .articleInnerWrap {
    border: 0 none;
  }
  #contact .box1of1 {
    padding: 3.5em 5em 3em 5em;
  }
  #contact .box1of1 li {
    display: inline;
    list-style: none;
  }
  #contact .box1of1 li > * {
    background: var(--background-primary-color);
    border-color: var(--primary-color);
    border-style: solid;
    border-width: 2px;
    border-radius: .25em;
    color: var(--primary-color);
    display: inline-block;
    font-size: 1.9rem;
    line-height: 1.75;
    margin: 0em .5em .5em 0em;
    padding: .25em 1em;
    text-align: center;
    text-decoration: none;
  }
</style>

* <a href='mailt&#111;&#58;%62m%6&#67;y%6Fn&#64;g%6&#68;%61&#105;l&#46;%63%6Fm'>&#98;m&#108;yon&#64;&#103;m&#97;&#105;l&#46;&#99;&#111;m</a>
* <a href="/content/bmlyon_resume.pdf" target="_blank" rel="noopener">Resume</a>
* <a href="https://www.linkedin.com/in/brandonmlyon/" target="_blank" rel="noopener">LinkedIn</a>
* <a href="https://gitlab.com/users/brandonmlyon/starred" target="_blank" rel="noopener">Git</a>
* <a href="https://twitter.com/brandon_m_lyon" target="_blank" rel="noopener">Twitter</a>
* <a href="https://www.pinterest.com/designbybrandon/" target="_blank" rel="noopener">Pinterest</a>
