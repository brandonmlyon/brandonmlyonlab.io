---
date: '2020-09-15 23:39:51'
title: How do browsers render websites?
titleCodeSafe: how-do-browsers-render-websites
description: Watch this video to see how a web browser turns code into a website.
permalink: content/how-do-browsers-render-websites/
canonical: https://about.brandonmlyon.com/content/how-do-browsers-render-websites/
previousUrl: /content/a-software-tester-walks-into-a-bar/
nextUrl: /content/recommended-books/
layoutClass: box1of1
primaryColor: "#f2c00e"
tags:
  - all-posts
---

# How Do Browsers Render Websites

<h2><small><em>This is something that every website developer should know</em></small></h2>

&nbsp;

<div class="videoContainer">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/SmE4OwHztCc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
