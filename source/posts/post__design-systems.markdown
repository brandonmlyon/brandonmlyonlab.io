---
date: '2020-09-15 03:30:57'
title: Design Systems, UI Kits, and Reusable Components
titleCodeSafe: design-systems
description: Design websites quickly from a reusable and preapproved kit of parts.
permalink: content/design-systems/
canonical: https://about.brandonmlyon.com/content/design-systems/
previousUrl: /content/works-well-with-others/
nextUrl: /content/navigation-redesign/
layoutClass: box1of1
primaryColor: "#f2c00e"
tags:
  - all-posts
---
# Design Systems, UI Kits, and Reusable Components

Design systems are important tools that facilitate velocity at scale and maintain a cohesive brand identity. They help implementors choose from a preapproved kit of parts when creating new projects. A good design system will define when, when not, why, why not, how, and how not to use a component or component group. A great system will also define code variable tokens, conversion best practices, and address accessibility. A design system should be a continuously updated project with dedicated resources.

<strong>Below are two examples of design systems that I helped implement.</strong>

<div style="font-size: 1.2em;"><a href="https://www.figma.com/file/M4U7ZO7dXrctKHv9MBOn1E/GitLab-Design-System-2021" target="_blank" rel="noopener" style="display: inline-block; vertical-align: top; width: 47%; margin-right: 5%;"><img style="width:100%;" src="https://res.cloudinary.com/bml/image/upload/v1656171925/brandonmlyon/design-system-gitlab.png"></a><a href="https://www.figma.com/file/A2QgSDcEwu85WXQSVEwsAj/Codefresh-UI-Kit-Marketing-Website-2022" target="_blank" rel="noopener" style="display: inline-block; vertical-align: top; width: 47%;"><img style="width:100%;" src="https://res.cloudinary.com/bml/image/upload/v1656171925/brandonmlyon/design-system-codefresh.png"></a></div>
