---
date: '2020-09-15 22:32:42'
title: Works well with others
titleCodeSafe: works-well-with-others
description: Why do people enjoy working with me? What can we achieve together?
permalink: content/works-well-with-others/
canonical: https://about.brandonmlyon.com/content/works-well-with-others/
previousUrl: /content/growth-marketing/
nextUrl: /content/design-systems/
layoutClass: box1of1
primaryColor: "#e41b5b"
tags:
  - all-posts
---
# Works well with others

As a senior level employee it's my job to build teams, train, nurture, and mentor. I hold workshops for multiple disciplines. I define hiring requirements and I conduct interviews. I evaluate tools we'll be using and I architect plans.

Full-stack developers are great collaborators. We bridge and align stakeholders, engineers, designers, customers, end-users, and others.

My experience ranges from being a lone wolf responsible for everything, to managing engineering and design teams at large companies with over 1500 people.
