---
date: '2020-09-14 23:01:14'
title: Analytics Dashboards
titleCodeSafe: analytics-dashboards
description: Learn how to measure the right things at the right time.
permalink: content/analytics-dashboards/
canonical: https://about.brandonmlyon.com/content/analytics-dashboards/
previousUrl: /content/a-b-testing-framework/
nextUrl: /content/marketing-automation/
layoutClass: box1of2
sidebar: true
primaryColor: "#e26700"
imageAltText: Screenshot of new navigation and dashboards on Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1649438914/brandonmlyon/cf-analytics.jpg
tags:
  - all-posts
---
# Analytics Dashboards

It's important to focus a dashboard around an actionable question. "What's the health of my site" is an unfocused question with many answers. Compare that with "how many ebooks have been downloaded this quarter?" The second question has a narrow focus driving predictable outcomes.

<a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/analytics-best-practices.md" target="_blank" rel="noopener">🔗 Analytics Best Practices</a>
