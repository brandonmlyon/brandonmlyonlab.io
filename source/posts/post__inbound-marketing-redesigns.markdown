---
date: '2020-09-15 17:07:53'
title: Design for inbound marketing
titleCodeSafe: design-for-inbound-marketing
description: You only get one brief chance to capture a visitor's attention.
permalink: content/inbound-marketing-redesigns/
canonical: https://about.brandonmlyon.com/content/inbound-marketing-redesigns/
previousUrl: /content/navigation-redesign/
nextUrl: /content/registration-funnels/
layoutClass: box1of2
sidebar: true
primaryColor: "#5a54a4"
imageAltText: Screenshot of the logged out question view on Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1635544122/brandonmlyon/ee-vqp2018q3.jpg
tags:
  - all-posts
---
# Design for inbound marketing

Most logged-out traffic on Experts Exchange originated from a Google SERP (search engine result page). I was repeatedly trusted to design and build these landing pages because of my attention to detail and SEO knowledge. During this redesign I increased clickthroughs for the primary call-to-action by switching to a single-column design, reducing distractions.
