---
date: '2020-10-06 16:46:39'
title: Workflow documentation
titleCodeSafe: workflow-documentation
description: Improve efficiency by asking the right questions at the right time.
permalink: content/workflow-documentation/
canonical: https://about.brandonmlyon.com/content/workflow-documentation/
opengraphImage: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/opengraph-workflow-templates.png
previousUrl: /content/about-this-website/
nextUrl: /content/my-pinterest/
layoutClass: box1of1
primaryColor: "#5a54a4"
tags:
  - all-posts
---
# Workflow documentation

Asking the right questions at the right time can improve outcomes. Below are my best practices for gathering project requirements and reviewing work.

* <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/merge-requests.md" target="_blank" rel="noopener">How to review code</a>
* <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/writing-issue-templates.md" target="_blank" rel="noopener">How to write issue templates</a>
* <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/.gitlab/issue_templates/website-bug-report.md" target="_blank" rel="noopener">Report a Bug</a>
* <a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/.gitlab/issue_templates/request-website-work.md" target="_blank" rel="noopener">Request a webpage update</a>
