---
date: '2020-09-15 17:51:07'
title: Mobile app development
titleCodeSafe: mobile-app-development
description: From planning to published.
permalink: content/mobile-app-development/
canonical: https://about.brandonmlyon.com/content/mobile-app-development/
previousUrl: /content/event-websites/
nextUrl: /content/apparel-e-commerce/
layoutClass: box1of2
sidebar: true
primaryColor: "#00a3e0"
imageAltText: Screenshot of Experts Exchange's mobile app
image: https://res.cloudinary.com/bml/image/upload/v1635544122/brandonmlyon/ee-mobileapp2019q2.jpg
tags:
  - all-posts
---
# Mobile apps generate value

I built three major revisions of the Experts Exchange mobile app. I researched common UI patterns and app-store best practices. Our apps were approved on our first try because we strictly followed Apple and Google UI guidelines. During this process we wrote APIs, developer documentation, and articles sharing what we learned.
