---
date: '2020-09-15 22:30:06'
title: Natural language UX
titleCodeSafe: natural-language-ux
description: Why is it important to use natural phrasing when creating a user experience?
permalink: content/natural-language-ux/
canonical: https://about.brandonmlyon.com/content/natural-language-ux/
previousUrl: /content/content-management-systems-and-blogs/
nextUrl: /content/event-websites/
layoutClass: box1of2
sidebar: true
primaryColor: "#5a54a4"
imageAltText: Screenshot of the question closing process on Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1635544122/brandonmlyon/ee-closing.jpg
tags:
  - all-posts
---
# Natural language user experiences

In order to improve metrics for Experts Exchange, we applied natural-language UX. We switched from one-word CTAs like *"answer"* to phrases like *"is this your solution?"*

To improve user satisfaction we swapped like & dislike buttons with "helpful" & "unhelpful" buttons. This improved clarity of purpose allowed us to collect actionable metrics for our behavior reporting system.
