---
date: '2020-09-15 20:26:25'
title: About this website
titleCodeSafe: about-this-website
description: How was this website designed and developed?
permalink: content/about-this-website/
canonical: https://about.brandonmlyon.com/content/about-this-website/
previousUrl: /content/about-me/
nextUrl: /content/workflow-documentation/
layoutClass: box1of1
primaryColor: "#E26700"
tags:
  - all-posts
---
# About this website

I'm currently planning to rebuild my portfolio with a more traditional landing page style. <a href="https://www.figma.com/file/aVJ8M0Rfz86n1YG5AdLvKC/Site-Content?node-id=0%3A1" target="_blank" rel="noopener">Click here to preview a rough wireframe</a> or <a href="https://careerfoundry.com/en/blog/ux-design/what-is-a-wireframe-guide/" target="_blank" rel="noopener">What is a wireframe?</a>.

I frequently re-build my portfolio in order to learn new techniques. It currently uses <a href="https://squido.org/" target="_blank" rel="noopener">Squido</a> and <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank" rel="noopener">GitLab Pages</a>. <a href="https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https%3A%2F%2Fabout.brandonmlyon.com%2F" target="_blank" rel="noopener">The Lighthouse score</a> is nearly 100 with no known <a href="https://wave.webaim.org/report#/https://about.brandonmlyon.com/" target="_blank" rel="noopener">accessibility errors</a>. My preferred testing platform is <a href="https://www.cypress.io/" target="_blank" rel="noopener">Cypress.io</a>.

I've built this site using <a href="https://middlemanapp.com/" target="_blank" rel="noopener">Middleman,</a> <a href="https://nuxtjs.org/" target="_blank" rel="noopener">Nuxt (Vue),</a> <a href="https://nextjs.org/" target="_blank" rel="noopener">Next.js (React),</a> <a href="https://svelte.dev/" target="_blank" rel="noopener">Svelte,</a> <a href="https://riot.js.org/" target="_blank" rel="noopener">Riot.js,</a> <a href="https://angular.io/" target="_blank" rel="noopener">Angular,</a> <a href="https://www.polymer-project.org/" target="_blank" rel="noopener">Polymer project,</a> and <a href="https://www.meteor.com/" target="_blank" rel="noopener">Meteor</a>.

I've integrated with dozens of different CMS like <a href="https://www.netlifycms.org/" target="_blank" rel="noopener">Netlify CMS,</a> <a href="http://wordpress.org/" target="_blank" rel="noopener">Wordpress,</a> <a href="https://ghost.org/" target="_blank" rel="noopener">Ghost,</a> <a href="https://getkirby.com/" target="_blank" rel="noopener">Kirby,</a> <a href="http://sanity.io/" target="_blank" rel="noopener">Sanity,</a> <a href="https://forestry.io/" target="_blank" rel="noopener">Forestry,</a> <a href="https://prismic.io/" target="_blank" rel="noopener">Prismic,</a> <a href="https://www.contentful.com/" target="_blank" rel="noopener">Contentful,</a> <a href="https://strapi.io/" target="_blank" rel="noopener">Strapi,</a> and <a href="https://www.datocms.com/" target="_blank" rel="noopener">Dato</a>.

The site has been hosted on <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank" rel="noopener">GitLab Pages,</a> <a href="https://www.netlify.com/" target="_blank" rel="noopener">Netlify,</a> <a href="https://firebase.google.com/" target="_blank" rel="noopener">Firebase,</a> <a href="https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/" target="_blank" rel="noopener">Bitbucket,</a> <a href="https://en.wikipedia.org/wiki/Virtual_private_server" target="_blank" rel="noopener">VPS,</a> and home-grown server hardware using <a href="https://www.docker.com/" target="_blank" rel="noopener">Docker,</a> <a href="https://www.turnkeylinux.org/core" target="_blank" rel="noopener">TurnKey Core Linux,</a> or <a href="https://www.vagrantup.com/" target="_blank" rel="noopener">Vagrant</a>.
