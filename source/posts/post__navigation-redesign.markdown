---
date: '2020-09-14 23:01:14'
title: Navigation redesign
titleCodeSafe: navigation-redesign
description: UX research reveals pain-points for end-users.
permalink: content/navigation-redesign/
canonical: https://about.brandonmlyon.com/content/navigation-redesign/
previousUrl: /content/design-systems/
nextUrl: /content/inbound-marketing-redesigns/
layoutClass: box1of2
sidebar: true
primaryColor: "#00a3e0"
imageAltText: Screenshot of new navigation and dashboards on Experts Exchange
image: https://res.cloudinary.com/bml/image/upload/v1635544122/brandonmlyon/ee-header2019q1.jpg
tags:
  - all-posts
---
# Navigation impacts everyone

Millions of people on Experts Exchange couldn't find what they needed. I analyzed navigation usage, conducted surveys, ran card-sorting exercises, and applied F.I.T. principles. To reduce complexity we moved less important items into new dashboards. We tailored unique menus for each user segment. To improve business, we prioritized links that impact subscriber retention.
