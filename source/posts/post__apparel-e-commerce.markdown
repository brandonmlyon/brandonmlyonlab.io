---
date: '2020-09-15 22:44:31'
title: Apparel e-commerce
titleCodeSafe: apparel-e-commerce
description: I built an e-commerce solution for a high-end apparel company.
permalink: content/apparel-e-commerce/
canonical: https://about.brandonmlyon.com/content/apparel-e-commerce/
previousUrl: /content/mobile-app-development/
nextUrl: /content/about-me/
layoutClass: box1of2
sidebar: true
primaryColor: "#f2c00e"
imageAltText: "Screenshot of Straight Down's website"
image: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/sd-home.jpg
tags:
  - all-posts
---
# High-end apparel e-commerce

Straight Down had a ten year old discontinued e-commerce application. I rebuilt their platform using Magento. There were hundreds of distinct apparel products with iterations of size, cut, material, and color. Sales and coupons were a regular occurrence. The website had to integrate with their shipping database. I added a Wordpress blog for product news and email subscriptions.
