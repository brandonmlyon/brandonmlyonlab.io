---
date: '2020-09-15 22:39:36'
title: Content management systems and blogs
titleCodeSafe: content-management-systems-and-blogs
description: I implement CMS websites on a variety of platforms.
permalink: content/content-management-systems-and-blogs/
canonical: https://about.brandonmlyon.com/content/content-management-systems-and-blogs/
previousUrl: /content/re-build-from-scratch/
nextUrl: /content/natural-language-ux/
layoutClass: box1of2
sidebar: true
primaryColor: "#f2c00e"
imageAltText: Screenshot of Experts Exchange's Blog
image: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/ee-blog.jpg
tags:
  - all-posts
---
# Custom themes, SEO, and security

It's important to connect with your users. I've built and rebuilt many CMS websites. One should always optimize the performance, security, and SEO of content management systems such as Wordpress.
