---
date: '2020-10-06 19:55:49'
title: Smarter A/B tests with custom tools
titleCodeSafe: smarter-a-b-tests-with-custom-tools
description: I create developer tools for testing conversion metrics.
permalink: content/a-b-testing-framework/
canonical: https://about.brandonmlyon.com/content/a-b-testing-framework/
previousUrl: /content/registration-funnels/
nextUrl: /content/analytics-dashboards/
layoutClass: box1of2
sidebar: true
primaryColor: "#f2c00e"
image: https://res.cloudinary.com/bml/image/upload/v1635544123/brandonmlyon/gl-featureflags.jpg
tags:
  - all-posts
---
# Smarter A/B tests with custom tools

GitLab's marketing department needed to measure the impact of changes to key conversion funnels. I researched needs, evaluated approaches, and chose a vendor. After that, I built tools that connect with LaunchDarkly for AB testing. Stakeholders were thrilled and appreciated the extra tools I created such as live test previews via URL parameter and a framework for partial include files.
