---
date: '2020-09-15 20:59:10'
title: Technical writing
titleCodeSafe: technical-writing
description: Read some of my articles about technology.
permalink: content/technical-writing/
canonical: https://about.brandonmlyon.com/content/technical-writing/
previousUrl: /content/my-twitter/
nextUrl: /content/project-management/
layoutClass: manual
manualLayout: true
primaryColor: "#5a54a4"
tags:
  - all-posts
---
<div class="box box1of1">
  <h1>Technical writing</h1>
  <p>I like to write. I've created documentation, articles, blog posts, case studies, research papers, brochures, whitepapers, and slideshares.</p>
  <ul>
    <li><a href="https://about.gitlab.com/blog/2019/11/06/how-to-stay-productive-in-your-home-office/" target="_blank" rel="noopener">How To Stay Productive Working Remotely</a></li>
    <li><a href="https://gitlab.com/brandon_lyon/documentation/-/blob/master/docs/css-best-practices.md" target="_blank" rel="noopener">CSS Best Practices</a></li>
    <li><a href="https://www.experts-exchange.com/articles/30601/Code-Responsibly.html" target="_blank" rel="noopener">Code Responsibly</a></li>
    <li><a href="https://www.experts-exchange.com/articles/18019/Before-you-decide-to-run-your-own-webserver.html" target="_blank" rel="noopener">Before You Decide To Run Your Own Webserver</a></li>
    <li><a href="https://www.experts-exchange.com/articles/19121/Life-with-an-Amazon-Echo.html" target="_blank" rel="noopener">Life With An Amazon Echo</a></li>
    <li><a href="https://www.experts-exchange.com/articles/28608/Mobile-App-Development-Best-Practices.html" target="_blank" rel="noopener">Mobile App Development Best Practices</a></li>
  </ul>
</div>
