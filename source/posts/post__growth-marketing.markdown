---
date: '2020-11-25 16:45:26'
title: Marketing and product growth
titleCodeSafe: growth-marketing
description: Not many people know how to improve leads, acquisition, conversion, and retention.
permalink: content/growth-marketing/
canonical: https://about.brandonmlyon.com/content/growth-marketing/
previousUrl: /content/not-just-a-developer/
nextUrl: /content/works-well-with-others/
layoutClass: box1of1
primaryColor: "#e26700"
tags:
  - all-posts
---
# I ❤️ marketing and product growth

I feel fortunate to have extensive knowledge in a rare niche when it comes to engineering and design. Marketing and growth are full of interesting challenges.

It's important to attract new leads, turn visitors into customers, grow their seat count, upgrade their feature tiers, and make them feel like a part of a community. You also have to improve satisfaction so users don't leave, and provide them with reasons to come back if they do leave.

Create value, communicate that value, and show appreciation to your customers.
