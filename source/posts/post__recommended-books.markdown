---
date: '2020-11-12 20:59:10'
title: Recommended books
titleCodeSafe: recommended-books
description: Level up your development skills.
permalink: content/recommended-books/
canonical: https://about.brandonmlyon.com/content/recommended-books/
previousUrl: /content/how-do-browsers-render-websites/
nextUrl: /content/last-post/
layoutClass: manual
manualLayout: true
primaryColor: "#5a54a4"
tags:
  - all-posts
---
<style type="text/css" media="screen">
  img[src="https://www.goodreads.com/images/widget/widget_logo.gif"] {
    display: none;
  }
  .gr_custom_container_1605546365 {
    /* customize your Goodreads widget container here*/
    border-radius:10px;
    font-size: 12px;
    padding: 10px 5px 10px 5px;
    background-color: #FFFFFF;
    color: #000000;
    display: flex;
    flex-wrap: wrap;
  }
  .gr_custom_container_1605546365 a {
    color: #222222;
    text-decoration: none;
  }
  .gr_custom_container_1605546365 a:active,
  .gr_custom_container_1605546365 a:focus,
  .gr_custom_container_1605546365 a:hover {
    text-decoration: underline;
  }
  .gr_custom_container_1605546365 * {
    line-height: 1.5em;
  }
  .gr_custom_header_1605546365 {
    /* customize your Goodreads header here*/
    display: none;
  }
  .gr_custom_each_container_1605546365 {
    /* customize each individual book container here */
    width: 100%;
    clear: both;
    margin-bottom: 10px;
    overflow: auto;
    padding-bottom: 4px;
    border-bottom: 1px solid #aaa;
    max-width: 300px;
    border: 0 none;
    box-shadow: 0px 0px 3px rgba(0,0,0,.3);
    margin: 0 20px 20px 0;
    padding: 10px;
    flex: 0 0 auto;
  }
  .gr_custom_book_container_1605546365 {
    /* customize your book covers here */
    overflow: hidden;
    height: 60px;
      float: left;
      margin-right: 4px;
      width: 39px;
  }
  .gr_custom_author_1605546365 {
    /* customize your author names here */
    font-size: 10px;
  }
  .gr_custom_tags_1605546365 {
    /* customize your tags here */
    font-size: 10px;
    color: gray;
  }
  .gr_custom_rating_1605546365 {
    /* customize your rating stars here */
    float: right;
  }
  .gr_custom_container_1605546365 .gr_custom_author_1605546365,
  .gr_custom_container_1605546365 .gr_custom_author_1605546365 a {
    color: #777777;
  }
  div[class*="custom_title"] {
    display: inline-block;
    width: calc(100% - 45px);
  }
</style>
<div class="box box1of1">
  <h1>Recommended books</h1>

<div id="gr_custom_widget_1605546365">
  <div class="gr_custom_container_1605546365">
    <h2 class="gr_custom_header_1605546365">
    <a style="text-decoration: none;" rel="nofollow" href="https://www.goodreads.com/review/list/2150046-brandon?shelf=development-design&amp;utm_medium=api&amp;utm_source=custom_widget">Design and Development</a>
    </h2>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Are Your Lights On?: How to Figure Out What the Problem Really is" rel="nofollow" href="https://www.goodreads.com/review/show/3647927030?utm_medium=api&amp;utm_source=custom_widget"><img alt="Are Your Lights On?: How to Figure Out What the Problem Really is" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1356474262l/1044831._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647927030?utm_medium=api&amp;utm_source=custom_widget">Are Your Lights On?: How to Figure Out What the Problem Really is</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/490213.Donald_C_Gause">Donald C. Gause</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="100 Things Every Designer Needs to Know about People" rel="nofollow" href="https://www.goodreads.com/review/show/3647933162?utm_medium=api&amp;utm_source=custom_widget"><img alt="100 Things Every Designer Needs to Know about People" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348700218l/10778139._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647933162?utm_medium=api&amp;utm_source=custom_widget">100 Things Every Designer Needs to Know about People</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/1893922.Susan_M_Weinschenk">Susan M. Weinschenk</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Humans vs Computers" rel="nofollow" href="https://www.goodreads.com/review/show/3647939560?utm_medium=api&amp;utm_source=custom_widget"><img alt="Humans vs Computers" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1503610919l/36118272._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647939560?utm_medium=api&amp;utm_source=custom_widget">Humans vs Computers</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/1407215.Gojko_Adzic">Gojko Adzic</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="When: The Scientific Secrets of Perfect Timing" rel="nofollow" href="https://www.goodreads.com/review/show/3647940128?utm_medium=api&amp;utm_source=custom_widget"><img alt="When: The Scientific Secrets of Perfect Timing" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1502223427l/35412097._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647940128?utm_medium=api&amp;utm_source=custom_widget">When: The Scientific Secrets of Perfect Timing</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/96150.Daniel_H_Pink">Daniel H. Pink</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Expect the Unexpected (or You Won't Find It): A Creativity Tool Based on the Ancient Wisdom of Heraclitus" rel="nofollow" href="https://www.goodreads.com/review/show/3647941171?utm_medium=api&amp;utm_source=custom_widget"><img alt="Expect the Unexpected (or You Won't Find It): A Creativity Tool Based on the Ancient Wisdom of Heraclitus" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1328747401l/134899._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647941171?utm_medium=api&amp;utm_source=custom_widget">Expect the Unexpected (or You Won't Find It): A Creativity Tool Based on the Ancient Wisdom of Heraclitus</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/77995.Roger_Von_Oech">Roger Von Oech</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="The Information: A History, a Theory, a Flood" rel="nofollow" href="https://www.goodreads.com/review/show/3647941840?utm_medium=api&amp;utm_source=custom_widget"><img alt="The Information: A History, a Theory, a Flood" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348046486l/8701960._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647941840?utm_medium=api&amp;utm_source=custom_widget">The Information: A History, a Theory, a Flood</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/10401.James_Gleick">James Gleick</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="If Hemingway Wrote JavaScript" rel="nofollow" href="https://www.goodreads.com/review/show/3647942140?utm_medium=api&amp;utm_source=custom_widget"><img alt="If Hemingway Wrote JavaScript" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1411952988l/21487480._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647942140?utm_medium=api&amp;utm_source=custom_widget">If Hemingway Wrote JavaScript</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/7456192.Angus_Croll">Angus Croll</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Contagious: Why Things Catch On" rel="nofollow" href="https://www.goodreads.com/review/show/3647942359?utm_medium=api&amp;utm_source=custom_widget"><img alt="Contagious: Why Things Catch On" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1376783352l/15801967._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647942359?utm_medium=api&amp;utm_source=custom_widget">Contagious: Why Things Catch On</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/1170746.Jonah_Berger">Jonah Berger</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Influencer: Building Your Personal Brand in the Age of Social Media" rel="nofollow" href="https://www.goodreads.com/review/show/3647942712?utm_medium=api&amp;utm_source=custom_widget"><img alt="Influencer: Building Your Personal Brand in the Age of Social Media" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1532385446l/36560239._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647942712?utm_medium=api&amp;utm_source=custom_widget">Influencer: Building Your Personal Brand in the Age of Social Media</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/17311491.Brittany_Hennessy">Brittany Hennessy</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Graphic Content: True Stories from Top Creatives" rel="nofollow" href="https://www.goodreads.com/review/show/3647943029?utm_medium=api&amp;utm_source=custom_widget"><img alt="Graphic Content: True Stories from Top Creatives" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1391413283l/18145490._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647943029?utm_medium=api&amp;utm_source=custom_widget">Graphic Content: True Stories from Top Creatives</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/656340.Brian_Singer">Brian Singer</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="The Elements of Persuasion: Use Storytelling to Pitch Better, Sell Faster  Win More Business" rel="nofollow" href="https://www.goodreads.com/review/show/3647943816?utm_medium=api&amp;utm_source=custom_widget"><img alt="The Elements of Persuasion: Use Storytelling to Pitch Better, Sell Faster  Win More Business" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348960692l/1670282._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647943816?utm_medium=api&amp;utm_source=custom_widget">The Elements of Persuasion: Use Storytelling to Pitch Better, Sell Faster  Win More Business</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/61986.Richard_Maxwell">Richard Maxwell</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Emotional Design: Why We Love (or Hate) Everyday Things" rel="nofollow" href="https://www.goodreads.com/review/show/3647943983?utm_medium=api&amp;utm_source=custom_widget"><img alt="Emotional Design: Why We Love (or Hate) Everyday Things" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1347523107l/841._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647943983?utm_medium=api&amp;utm_source=custom_widget">Emotional Design: Why We Love (or Hate) Everyday Things</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/552.Donald_A_Norman">Donald A. Norman</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Seductive Interaction Design: Creating Playful, Fun, and Effective User Experiences" rel="nofollow" href="https://www.goodreads.com/review/show/3647947410?utm_medium=api&amp;utm_source=custom_widget"><img alt="Seductive Interaction Design: Creating Playful, Fun, and Effective User Experiences" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348360985l/9967766._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647947410?utm_medium=api&amp;utm_source=custom_widget">Seductive Interaction Design: Creating Playful, Fun, and Effective User Experiences</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/4523557.Stephen_P_Anderson">Stephen P. Anderson</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Designing for People" rel="nofollow" href="https://www.goodreads.com/review/show/3647948336?utm_medium=api&amp;utm_source=custom_widget"><img alt="Designing for People" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1388262230l/1604762._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647948336?utm_medium=api&amp;utm_source=custom_widget">Designing for People</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/743875.Henry_Dreyfuss">Henry Dreyfuss</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="The Designer Says: Quotes, Quips, and Words of Wisdom (gift book with inspirational quotes for designers, fun for team building and creative motivation)" rel="nofollow" href="https://www.goodreads.com/review/show/3647949864?utm_medium=api&amp;utm_source=custom_widget"><img alt="The Designer Says: Quotes, Quips, and Words of Wisdom" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1400655935l/15791237._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647949864?utm_medium=api&amp;utm_source=custom_widget">The Designer Says: Quotes, Quips, and Words of Wisdom</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/239478.Sara_Bader">Sara Bader</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Antifragile: Things That Gain from Disorder" rel="nofollow" href="https://www.goodreads.com/review/show/3647952795?utm_medium=api&amp;utm_source=custom_widget"><img alt="Antifragile: Things That Gain from Disorder" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1352422827l/13530973._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647952795?utm_medium=api&amp;utm_source=custom_widget">Antifragile: Things That Gain from Disorder</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/21559.Nassim_Nicholas_Taleb">Nassim Nicholas Taleb</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Principles: Life and Work" rel="nofollow" href="https://www.goodreads.com/review/show/3647953093?utm_medium=api&amp;utm_source=custom_widget"><img alt="Principles: Life and Work" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1503365703l/34536488._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647953093?utm_medium=api&amp;utm_source=custom_widget">Principles: Life and Work</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/5289593.Ray_Dalio">Ray Dalio</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Design for the Mind: Seven Psychological Principles of Persuasive Design" rel="nofollow" href="https://www.goodreads.com/review/show/3647953317?utm_medium=api&amp;utm_source=custom_widget"><img alt="Design for the Mind: Seven Psychological Principles of Persuasive Design" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1469126730l/31191083._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647953317?utm_medium=api&amp;utm_source=custom_widget">Design for the Mind: Seven Psychological Principles of Persuasive Design</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/14046445.Victor_S_Yocco">Victor S. Yocco</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Actionable Gamification: Beyond Points, Badges, and Leaderboards" rel="nofollow" href="https://www.goodreads.com/review/show/3647953511?utm_medium=api&amp;utm_source=custom_widget"><img alt="Actionable Gamification: Beyond Points, Badges, and Leaderboards" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1437479451l/25416321._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647953511?utm_medium=api&amp;utm_source=custom_widget">Actionable Gamification: Beyond Points, Badges, and Leaderboards</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/13572095.Yu_kai_Chou">Yu-kai Chou</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Bottlenecks: Aligning UX Design with User Psychology" rel="nofollow" href="https://www.goodreads.com/review/show/3647954745?utm_medium=api&amp;utm_source=custom_widget"><img alt="Bottlenecks: Aligning UX Design with User Psychology" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1488382702l/33268103._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647954745?utm_medium=api&amp;utm_source=custom_widget">Bottlenecks: Aligning UX Design with User Psychology</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/617265.David_C_Evans">David C. Evans</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="The Culture Map: Breaking Through the Invisible Boundaries of Global Business" rel="nofollow" href="https://www.goodreads.com/review/show/3647955446?utm_medium=api&amp;utm_source=custom_widget"><img alt="The Culture Map: Breaking Through the Invisible Boundaries of Global Business" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1488319495l/22085568._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647955446?utm_medium=api&amp;utm_source=custom_widget">The Culture Map: Breaking Through the Invisible Boundaries of Global Business</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/7178109.Erin_Meyer">Erin Meyer</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Drive: The Surprising Truth About What Motivates Us" rel="nofollow" href="https://www.goodreads.com/review/show/3647956197?utm_medium=api&amp;utm_source=custom_widget"><img alt="Drive: The Surprising Truth About What Motivates Us" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348931599l/6452796._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647956197?utm_medium=api&amp;utm_source=custom_widget">Drive: The Surprising Truth About What Motivates Us</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/96150.Daniel_H_Pink">Daniel H. Pink</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Microinteractions: Designing with Details" rel="nofollow" href="https://www.goodreads.com/review/show/3647956484?utm_medium=api&amp;utm_source=custom_widget"><img alt="Microinteractions: Designing with Details" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1365526301l/17239285._SX50_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647956484?utm_medium=api&amp;utm_source=custom_widget">Microinteractions: Designing with Details</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/1242.Dan_Saffer">Dan Saffer</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration" rel="nofollow" href="https://www.goodreads.com/review/show/3647957475?utm_medium=api&amp;utm_source=custom_widget"><img alt="Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1400863577l/18077903._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647957475?utm_medium=api&amp;utm_source=custom_widget">Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/5618463.Ed_Catmull">Ed Catmull</a>
        </div>
    </div>
    <div class="gr_custom_each_container_1605546365">
        <div class="gr_custom_book_container_1605546365">
          <a title="Predictably Irrational: The Hidden Forces That Shape Our Decisions" rel="nofollow" href="https://www.goodreads.com/review/show/3647957734?utm_medium=api&amp;utm_source=custom_widget"><img alt="Predictably Irrational: The Hidden Forces That Shape Our Decisions" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1255573980l/1713426._SY75_.jpg" /></a>
        </div>
        <div class="gr_custom_title_1605546365">
          <a rel="nofollow" href="https://www.goodreads.com/review/show/3647957734?utm_medium=api&amp;utm_source=custom_widget">Predictably Irrational: The Hidden Forces That Shape Our Decisions</a>
        </div>
        <div class="gr_custom_author_1605546365">
          by <a rel="nofollow" href="https://www.goodreads.com/author/show/788461.Dan_Ariely">Dan Ariely</a>
        </div>
    </div>
    <br style="clear: both"/>
    <center>
      <a rel="nofollow" href="https://www.goodreads.com/"><img alt="goodreads.com" style="border:0" src="https://www.goodreads.com/images/widget/widget_logo.gif" /></a>
    </center>
    <noscript>
      Share <a rel="nofollow" href="https://www.goodreads.com/">book reviews</a> and ratings with Brandon, and even join a <a rel="nofollow" href="https://www.goodreads.com/group">book club</a> on Goodreads.
    </noscript>
  </div>
</div>
<script src="https://www.goodreads.com/review/custom_widget/2150046.Design%20and%20Development?cover_position=left&cover_size=small&num_books=100&order=a&shelf=development-design&show_author=1&show_cover=1&show_rating=0&show_review=0&show_tags=0&show_title=1&sort=date_added&widget_bg_color=FFFFFF&widget_bg_transparent=&widget_border_width=1&widget_id=1605546365&widget_text_color=000000&widget_title_size=medium&widget_width=medium" type="text/javascript" charset="utf-8"></script>

