---
date: '2020-09-15 22:41:58'
title: A software tester walks into a bar
titleCodeSafe: a-software-tester-walks-into-a-bar
description: What happens when a QA engineer is set up with a familiar joke?
permalink: content/a-software-tester-walks-into-a-bar/
canonical: https://about.brandonmlyon.com/content/a-software-tester-walks-into-a-bar/
previousUrl: /content/the-door-problem/
nextUrl: /content/how-do-browsers-render-websites/
layoutClass: box1of1 boxQuote
primaryColor: "#00a3e0"
tags:
  - all-posts
---
## ***Recommended reading***

"Orders a beer. Orders 0 beers. Orders 999999999 beers. Orders a lizard. Orders -1 beers. Orders a sfdeljknesv."

<a href="https://www.sempf.net/post/On-Testing1" target="_blank" rel="noopener">🔗 A software tester walks into a bar</a>
