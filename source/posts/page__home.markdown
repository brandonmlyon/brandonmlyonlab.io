---
date: '2220-09-14 23:01:14'
title: is a developer, designer, and manager.
titleCodeSafe: is-a-developer-designer-and-manager
description: "I'm a full stack developer and designer. Websites are my passion but I also do graphic design, apparel, industrial, architectural, and photography."
permalink: content/home/
canonical: https://about.brandonmlyon.com/
layoutClass: box1of1
primaryColor: '#e41b5b'
nextUrl: '/content/not-just-a-developer/'
---

<h1 style="">I know how to improve your project</h1>

<h2 class="smaller">You need full stack development, design, and product management focused on growth and user experience.</h2>

*Scroll down to learn more about Brandon M Lyon and his work*