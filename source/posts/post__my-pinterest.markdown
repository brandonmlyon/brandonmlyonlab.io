---
date: '2020-09-15 22:54:20'
title: My Pinterest
titleCodeSafe: my-pinterest
description: "These mood boards show designs and details that I like."
permalink: content/my-pinterest/
canonical: https://about.brandonmlyon.com/content/my-pinterest/
previousUrl: /content/workflow-documentation/
nextUrl: /content/my-twitter/
layoutClass: box1of1
primaryColor: "#f2c00e"
tags:
  - all-posts
---
<style>
  #my-pinterest .box1of1 {max-width: 1380px;}
  .moodboards .listItem {
    display: inline-block;
    height: 430px;
    margin: 0 2em 2em 0;
    width: calc(50% - 2em - 2em);
  }
  @media (max-width: 639px) {
    .moodboards .listItem {
      margin-right: 0;
      width: 100%;
    }
  }
</style>

<h1>My Pinterest</h1>

<p>These mood boards show designs and details that I find applicable to my work.</p>

<div class="moodboards">
  <div class="listItem">
    <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/website-design-inspiration/"></a>
  </div>
  <div class="listItem">
    <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/mobile-design-inspiration/"></a>
  </div>
  <div class="listItem">
    <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/graphic-design-inspiration/"></a>
  </div>
  <div class="listItem">
    <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/colorways/"></a>
  </div>
</div>

<script>
  var showPins = function() {
    var script = document.createElement('script');
    var prior = document.getElementsByTagName('script')[0];
    script.async = 1;
    prior.parentNode.insertBefore(script, prior);
    script.src = 'https://assets.pinterest.com/js/pinit_main.js';
  };
  showPins();
</script>
