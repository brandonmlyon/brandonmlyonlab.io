const config = {
    test: {
        name: 'Brandon M Lyon',
        description: 'Portfolio',
        twitterHandle: '@brandon_m_lyon',
        baseUrl: 'http://localhost:4965',
        sourcesExt: 'markdown',
        sourceDir: 'source',
        buildDir: 'public',
        summaryLength: 250,
        port: 4965,
        pagination: false,
        postPerPage: 8,
        plugins: [
            {
                name: 'testplugin',
                options: {
                    setting: true,
                    another: false
                }
            },
            {
                name: 'dudplugin',
                options: {
                    setting: true,
                    another: false
                }
            }
        ],
        postBuild: [
            {
                name: 'zip',
                options: {}
            }
        ]
    },
    development: {
        name: 'Brandon M Lyon',
        description: 'Portfolio',
        twitterHandle: '@brandon_m_lyon',
        baseUrl: 'http://localhost:4965',
        sourcesExt: 'markdown',
        templateEngine: 'hbs',
        templateConfig: {},
        sourceDir: 'source',
        buildDir: 'public',
        summaryLength: 250,
        port: 4965,
        pagination: false,
        postPerPage: 8,
        postBuild: [
            {
                name: 'zip',
                options: {}
            }
        ]
    },
    production: {
        name: 'Brandon M Lyon',
        description: 'Portfolio',
        twitterHandle: '@brandon_m_lyon',
        baseUrl: 'https://about.brandonmlyon.com/',
        sourcesExt: 'markdown',
        sourceDir: 'source',
        buildDir: 'public',
        summaryLength: 250,
        port: 4965,
        pagination: false,
        postPerPage: 8
    }
};

module.exports = config;
