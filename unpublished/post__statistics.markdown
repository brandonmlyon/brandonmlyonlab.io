---
date: '2020-09-15 03:30:57'
title: Statistics
titleCodeSafe: statistics
description: Some people prefer numbers. Review the quantitative assessment of my career.
permalink: content/statistics/
canonical: https://about.brandonmlyon.com/content/statistics/
previousUrl: /content/works-well-with-others/
nextUrl: /content/navigation-redesign/
layoutClass: box1of1
primaryColor: "#f2c00e"
tags:
  - all-posts
---
<style>
#statistics p {
  margin-bottom: .5em;
}
#statistics ul {
  margin-bottom: 4em;
}
#statistics li {
  margin-left: 2em;
}
</style>
# Brandon has supported...

<p><strong>Billions of</strong></p>
<ul>
<li>USD of ARR by leading engineering and design for marketing websites</li>
</ul>
<p><strong>Millions of</strong></p>
<ul>
<li>Visitors served</li>
<li>Pieces of user generated content</li>
</ul>
<p><strong>Hundreds of Thousands of</strong></p>
<ul>
<li>Lines of code written</li>
<li>Paid users served</li>
</ul>
<p><strong>Hundreds of</strong></p>
<ul>
<li>Coworkers collaborated with</li>
<li>Projects lead</li>
<li>A/B tests run</li>
</ul>
<p><strong>Dozens of</strong></p>
<ul>
<li>Engineers mentored</li>
<li>Designers mentored</li>
</ul>
