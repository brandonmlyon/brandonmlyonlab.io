This is built using [Squido](https://github.com/mrvautin/squido)

To install Squido

`npm i -g https://github.com/mrvautin/squido.git`

To build and run the development server

`squido serve -b -w -c`

## Colors

* #e41b5b pink
* #00a3e0 blue
* #f2c00e yellow
* #5a54a4 purple
* #e26700 orange
* #cccccc gray
